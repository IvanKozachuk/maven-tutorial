FROM maven:3.6-jdk-11

WORKDIR /usr/src/app

COPY /docker-context/maven-tutorial-1.0-SNAPSHOT.jar .

CMD ['java', '-jar', 'maven-tutorial-1.0-SNAPSHOT.jar']